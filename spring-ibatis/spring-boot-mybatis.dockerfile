# pull official base image
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar

# add app
COPY ${JAR_FILE} spring-boot-mybatis-api.jar

# start app
CMD ["java", "-jar", "/spring-boot-mybatis-api.jar"]

EXPOSE 8080
