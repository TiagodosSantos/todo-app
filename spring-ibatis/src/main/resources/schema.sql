create table todo
(
   id integer not null,
   title varchar(255) not null,
   completed BOOLEAN default FALSE,
   primary key(id)
);