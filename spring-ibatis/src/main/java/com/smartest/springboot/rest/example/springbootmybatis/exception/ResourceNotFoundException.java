package com.smartest.springboot.rest.example.springbootmybatis.exception;


/**
 * 
 * This class represents the exception thrown as a result of data or resource not found. 
 * 
 * @author Tiago Santos
 * @since   2020-09-22
 * 
 */
public class ResourceNotFoundException extends Exception {


	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(final String message) {
		super(message);
	}

}
