package com.smartest.springboot.rest.example.springbootmybatis.controller;

import com.smartest.springboot.rest.example.springbootmybatis.model.Todo;
import com.smartest.springboot.rest.example.springbootmybatis.repository.TodoMyBatisRepository;
import com.smartest.springboot.rest.example.springbootmybatis.exception.ResourceNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

/**
 * 
 * Controller responsible for handling todo information
 * 
 * @author Tiago Santos
 * @since   2020-08-21
 * 
 */
@RestController
@RequestMapping(value = "/todos")
@Validated
public class TodoController {
	
	private static final Logger logger = LoggerFactory.getLogger(TodoController.class);
	
	@Autowired
	private TodoMyBatisRepository todoMyBatisRepository;
	
	
	/**
	   * This is  method returns all todos from the database
	   * 
	   * @return list with all todos
	   * @exception Exception on general errors.
	   * @see Todo
	   */
	@ApiOperation(value = "Return a list with all todos of the database", response = Todo.class)
	@ApiResponses({
			@ApiResponse(code = 500, message = "Internal Server Error")})
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Todo> findAll() {
		
		logger.info("Start");
		
		Iterable<Todo> findAll = todoMyBatisRepository.findAll();
		logger.info("All todos?  -> "+findAll);

		logger.info("End");	
		return (List<Todo>) findAll;
	}
	
	/**
	   * This is  method find a specific todo by id
	   * 
	   * @return todo
	 * @throws Exception 
	   * @exception Exception on general errors and not found.
	   * @see Todo
	   */
	@ApiOperation(value = "Find and return todo by id", response = Todo.class)
	@ApiResponses({
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Todo not found")})
	@GetMapping("/{id}")
	public ResponseEntity<Todo> findByTodoId(@PathVariable @Min(1) Integer id) throws Exception {
		
		logger.info("Start");
		
		Todo todo = todoMyBatisRepository.findById(id);
		logger.info("todos?  -> "+ todo);
		
		ResponseEntity<Todo> response = null;
		if (todo != null)
			response = ResponseEntity.ok(todo);
		else
			//response = ResponseEntity.notFound().build();
			throw new ResourceNotFoundException("Todo não encontrado");
		
		logger.info("End");	
		return response;
	}
	
	/**
	   * This method is responsible for updating todo by id
	   * 
	   * @param id identification of the todo that needs to be updated.
	   * @param todo todo data that needs to be updated
	   * @return Todo updated if no exception is thrown.
	 * @throws Exception 
	   * @exception Exception on general errors.
	   * @see Todo
	   */
	@ApiOperation(value = "Update todo by id")
	@ApiResponses({
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Todo not found")})
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Todo> update(@PathVariable @Min(1) Integer id,
			/* @Valid */ @RequestBody Todo todo, UriComponentsBuilder uriBuilder) throws Exception {
		
		logger.info("Start");
		
		Todo todoFound = todoMyBatisRepository.findById(id);
		ResponseEntity<Todo> response = null;
		
			if (todoFound != null) {
				try {
					int rowsUpdated = todoMyBatisRepository.update(todo);
					if(rowsUpdated > 0)
						todoFound = todoMyBatisRepository.findById(id);
					else
						throw new ResourceNotFoundException("Todo não atualizado");
					response = ResponseEntity.ok(todoFound);
					logger.info("TodoToUpdate -> " + todoFound);
				}catch(Exception e) {
					logger.error("Erro while updating todo -> " + e.getCause());
					throw e; 
				}
			} else {
				//response = ResponseEntity.notFound().build();
				logger.info("Todo not found. TodoID -> " + id);
				throw new ResourceNotFoundException("Todo não encontrado");
			}
		
		
		logger.info("End");
		return response;
		
	}


	/**
	 * This method is responsible for insert a new todo
	 *
	 * @param todo todo data that needs to be inserted
	 * @return Todo created if no exception is thrown.
	 * @throws Exception
	 * @exception Exception on general errors.
	 * @see Todo
	 */
	@ApiOperation(value = "Insert a todo")
	@ApiResponses({
			@ApiResponse(code = 500, message = "Internal Server Error")})
	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Transactional
	public ResponseEntity<Todo> insert(@Valid @RequestBody Todo todo, UriComponentsBuilder uriBuilder) throws Exception {

		logger.info("Start");
		ResponseEntity<Todo> response = null;

			try {

				int rowsInserted = todoMyBatisRepository.insert(todo);
				Todo todoInserted = null;
				if(rowsInserted > 0)
					todoInserted = todoMyBatisRepository.findById(todo.getId());
				else
					throw new ResourceNotFoundException("Todo não atualizado");
				response = ResponseEntity.ok(todoInserted);
				logger.info("TodoToUpdate -> " + todoInserted);
			}catch(Exception e) {
				logger.error("Erro while updating todo -> " + e.getCause());
				throw e;
			}


		logger.info("End");
		return response;

	}
	

	/**
	   * This is method is responsible for deleting todos from the database
	   * 
	   * @param idTodo an integer that represents the todo id.
	   * @return ResponseEntity code.
	 * @throws Exception 
	   * @exception Exception on general errors.
	   */
	@ApiOperation(value = "Delete todo by id")
	@ApiResponses({
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Todo not found")})
	@DeleteMapping("/{idTodo}")
	@Transactional
	public ResponseEntity<?> remove(@PathVariable("idTodo") @Min(1) Integer idTodo) throws Exception {
		logger.info("Start");

		Todo todo = todoMyBatisRepository.findById(idTodo);
		ResponseEntity<?> response = null;
		
		if (todo != null) {
			todoMyBatisRepository.deleteById(idTodo);
			response = ResponseEntity.ok().build();
			logger.info("TodoDeleted  -> "+ todo);
		}else {
			//response = ResponseEntity.notFound().build();
			logger.info("TodoId "+idTodo+" not found");
			throw new ResourceNotFoundException("Todo não encontrado");
		}
		
		logger.info("End");
		return response;
	}
	
	
}
