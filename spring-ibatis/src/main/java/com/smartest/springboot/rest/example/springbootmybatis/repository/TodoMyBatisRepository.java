package com.smartest.springboot.rest.example.springbootmybatis.repository;

import java.util.List;

import com.smartest.springboot.rest.example.springbootmybatis.model.Todo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TodoMyBatisRepository {

    @Select("select * from todo")
    List<Todo> findAll();

    @Select("SELECT * FROM todo WHERE id = #{id}")
    Todo findById(int id);

    @Delete("DELETE FROM todo WHERE id = #{id}")
    int deleteById(int id);

    @Insert("INSERT INTO todo(id, title, completed) VALUES (#{id}, #{title}, #{completed})")
    int insert(Todo todo);

    @Update("Update todo set id=#{id}, title=#{title}, completed=#{completed} where id=#{id}")
    int update(Todo todo);
}
