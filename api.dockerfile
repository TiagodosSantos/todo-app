# pull official base image
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=api/*.jar

# add app
COPY ${JAR_FILE} api.jar

# start app
CMD ["java", "-jar", "/api.jar"]

EXPOSE 8080
