import React, { useState } from "react";
import { useActions } from "easy-peasy";

const AddTodo = () => {
  const [title, setTitle] = useState("");

  const add = useActions((actions) => actions.add);

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          add({
            id: Math.floor(Math.random() * 1000) + 1,
            title,
            completed: false,
          });
          setTitle("");
        }}
      >
        <input
          type="text"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          placeholder="Add todo title..."
        />
        <input type="submit" value="Add Todo" />
      </form>
    </div>
  );
};

export default AddTodo;
