import { action, thunk } from "easy-peasy";
import axios from "axios";

const api = axios.create({
  baseURL: `http://localhost:8080/api/todos`,
});

export default {
  todos: [],
  // Thunks
  fetchTodos: thunk(async (actions) => {
    try {
      const todos = await api.get(`/`).then((res) => res.data);
      actions.setTodos(todos);
    } catch (e) {
      console.log(e);
    }
  }),

  add: thunk(async (actions, payload) => {
    try {
      await api.post(`/`, payload).then((res) => res.data);
      actions.fetchTodos();
    } catch (e) {
      console.log(e);
    }
  }),

  toggle: thunk(async (actions, payload) => {
    try {
      console.log(payload);
      await api
        .put(`/${payload.id}`, payload)
        .then((res) => console.log(res.data));
      actions.fetchTodos();
    } catch (e) {
      console.log(e);
    }
  }),

  remove: thunk(async (actions, payload) => {
    try {
      await api.delete(`/${payload}`).then((res) => console.log(res));
      actions.fetchTodos();
    } catch (e) {
      console.log(e);
    }
  }),

  // Actions
  setTodos: action((state, todos) => {
    state.todos = todos;
  }),
};
